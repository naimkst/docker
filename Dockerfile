FROM alpine:latest

RUN apk update && apk upgrade
RUN apk add php
COPY . /app
WORKDIR /app
EXPOSE 8000
CMD php -S 0.0.0.0:5000

